const hamburger = document.querySelector("#hamburger")

let open = false;

hamburger.addEventListener("click", function (e) {
    e.preventDefault()
    
    open = !open

    if(open){
        this.style.backgroundImage = "url('../images/icon-close.svg')"
    }
    else{
        this.style.backgroundImage = "url('../images/icon-hamburger.svg')"
    }

    let target = document.querySelector(".mobile_nav")
    target.classList.toggle("active")
})
